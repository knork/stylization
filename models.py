import torch
from collections import namedtuple
from torchvision import models
import torch.nn as nn
import torch.nn.functional as F


class VGG16(torch.nn.Module):
    def __init__(self, requires_grad=False):
        super(VGG16, self).__init__()
        vgg_pretrained_features = models.vgg16(pretrained=True).features
        self.relu1_1 = torch.nn.Sequential()
        self.relu1_2 = torch.nn.Sequential()

        self.relu2_1 = torch.nn.Sequential()
        self.relu2_2 = torch.nn.Sequential()

        self.relu3_1 = torch.nn.Sequential()
        self.relu3_2 = torch.nn.Sequential()
        self.relu3_3 = torch.nn.Sequential()

        self.relu4_1 = torch.nn.Sequential()
        self.relu4_2 = torch.nn.Sequential()
        self.relu4_3 = torch.nn.Sequential()

        self.relu5_1 = torch.nn.Sequential()
        self.relu5_2 = torch.nn.Sequential()
        self.relu5_3 = torch.nn.Sequential()

        #self.norm1 = torch.nn.BatchNorm2d(30)
        self.norm1 = torch.nn.InstanceNorm2d(30)

        for x in range(2):
            self.relu1_1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(2,4):
            self.relu1_2.add_module(str(x), vgg_pretrained_features[x])

        for x in range(4, 7):
            self.relu2_1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(7, 9):
            self.relu2_2.add_module(str(x), vgg_pretrained_features[x])

        for x in range(9, 12):
            self.relu3_1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(12, 14):
            self.relu3_2.add_module(str(x), vgg_pretrained_features[x])
        for x in range(14, 16):
            self.relu3_3.add_module(str(x), vgg_pretrained_features[x])

        for x in range(16, 19):
            self.relu4_1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(19, 21):
            self.relu4_2.add_module(str(x), vgg_pretrained_features[x])
        for x in range(21, 23):
            self.relu4_3.add_module(str(x), vgg_pretrained_features[x])


        for x in range(23, 26):
            self.relu5_1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(26, 28):
            self.relu5_2.add_module(str(x), vgg_pretrained_features[x])
        for x in range(28, 30):
            self.relu5_3.add_module(str(x), vgg_pretrained_features[x])

        if not requires_grad:
            for param in self.parameters():
                param.requires_grad = False

    def forward(self, X):
        relu1_1 = self.relu1_1(X)
        relu1_2 = self.relu1_2 (relu1_1)

        relu2_1 = self.relu2_1(relu1_2)
        relu2_2 = self.relu2_2(relu2_1)

        relu3_1 = self.relu3_1(relu2_2)
        relu3_2 = self.relu3_2(relu3_1)
        relu3_3 = self.relu3_3(relu3_2)

        relu4_1 = self.relu4_1(relu3_3)
        relu4_2 = self.relu4_2(relu4_1)
        relu4_3 = self.relu4_3(relu4_2)

        relu5_1 = self.relu5_1(relu4_3)
        relu5_2 = self.relu5_2(relu5_1)
        relu5_3 = self.relu5_3(relu5_2)

        vgg_outputs = namedtuple("VggOutputs", ['relu1_1','relu1_2','relu2_1','relu2_2','relu3_1','relu3_2','relu3_3',
                                                'relu4_1','relu4_2','relu4_3','relu5_1','relu5_2','relu5_3'])
        out = vgg_outputs(relu1_1,relu1_2,relu2_1,relu2_2,relu3_1,relu3_2,relu3_3,relu4_1,relu4_2,relu4_3,relu5_1,relu5_2,relu5_3)
        return out


class TransformerNet(torch.nn.Module):
    def __init__(self):
        super(TransformerNet, self).__init__()
        self.model = nn.Sequential(
            ConvBlock(3, 32, kernel_size=9, stride=1),
            ConvBlock(32, 64, kernel_size=3, stride=2),
            ConvBlock(64, 128, kernel_size=3, stride=2),
            ResidualBlock(128),
            ResidualBlock(128),
            ResidualBlock(128),
            ResidualBlock(128),
            ResidualBlock(128),
            ConvBlock(128, 64, kernel_size=3, upsample=True),
            ConvBlock(64, 32, kernel_size=3, upsample=True),
            ConvBlock(32, 3, kernel_size=9, stride=1, normalize=False, relu=False),
        )

    def forward(self, x):
        return self.model(x)


class ResidualBlock(torch.nn.Module):
    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.block = nn.Sequential(
            ConvBlock(channels, channels, kernel_size=3, stride=1, normalize=True, relu=True),
            ConvBlock(channels, channels, kernel_size=3, stride=1, normalize=True, relu=False),
        )

    def forward(self, x):
        return self.block(x) + x


class ConvBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, upsample=False, normalize=True, relu=True):
        super(ConvBlock, self).__init__()
        self.upsample = upsample
        self.block = nn.Sequential(
            nn.ReflectionPad2d(kernel_size // 2), nn.Conv2d(in_channels, out_channels, kernel_size, stride)
        )
        self.norm = nn.InstanceNorm2d(out_channels, affine=True) if normalize else None
        self.relu = relu

    def forward(self, x):
        if self.upsample:
            x = F.interpolate(x, scale_factor=2)
        x = self.block(x)
        if self.norm is not None:
            x = self.norm(x)
        if self.relu:
            x = F.relu(x)
        return x
