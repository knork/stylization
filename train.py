import argparse
import os
import sys
import random
from PIL import Image
import numpy as np
import torch
import glob
from torch.optim import Adam
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.utils import save_image
from models import TransformerNet, VGG16
from utils import *
import matplotlib.pyplot as plt

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parser for Fast-Neural-Style")
    parser.add_argument("--dataset_path", type=str, required=True, help="path to training dataset")
    parser.add_argument("--style_image", type=str, default="style-images/mosaic.jpg", help="path to style image")
    parser.add_argument("--epochs", type=int, default=1, help="Number of training epochs")
    parser.add_argument("--batch_size", type=int, default=4, help="Batch size for training")
    parser.add_argument("--image_size", type=int, default=256, help="Size of training images")
    parser.add_argument("--style_size", type=int, help="Size of style image")
    parser.add_argument("--lambda_content", type=float, default=1e5, help="Weight for content loss")
    parser.add_argument("--lambda_style", type=float, default=1e10, help="Weight for style loss")
    parser.add_argument("--lr", type=float, default=1e-3, help="Learning rate")
    parser.add_argument("--checkpoint_model", type=str, help="Optional path to checkpoint model")
    parser.add_argument("--checkpoint_interval", type=int, default=500, help="Batches between saving model")
    parser.add_argument("--sample_interval", type=int, default=200, help="Batches between saving image samples")
    parser.add_argument("--content_layer", type=str, default='relu2_2', help="layers used for content loss")
    parser.add_argument("--style_layer", type=str, default="relu1_2,relu2_2,relu3_3,relu4_3",
                        help="layers used for style loss")

    args = parser.parse_args()
    content_losses = []
    style_losses = []

    folder_version = "no_norm"

    style_name = args.style_image.split("/")[-1].split(".")[0]
    unique_subfolder=f"c - {args.content_layer} - s - {args.style_layer} {folder_version}".strip('[] ')
    os.makedirs(f"images/outputs/{style_name}-{unique_subfolder}", exist_ok=True)

    os.makedirs(f"checkpoints/{style_name}-{unique_subfolder}/", exist_ok=True)
    os.makedirs(f"images/graphs/{style_name}-{unique_subfolder}/",exist_ok=True)
    args.content_layer = args.content_layer.split(',')
    args.style_layer = args.style_layer.split(',')

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Create dataloader for the training data
    train_dataset = datasets.ImageFolder(args.dataset_path, train_transform(args.image_size))
    dataloader = DataLoader(train_dataset, batch_size=args.batch_size)
    dataloader = DataLoader(train_dataset, batch_size=args.batch_size)

    # Defines networks
    transformer = TransformerNet().to(device)
    vgg = VGG16(requires_grad=False).to(device)

    # Load checkpoint model if specified
    if args.checkpoint_model:
        transformer.load_state_dict(torch.load(args.checkpoint_model))

    # Define optimizer and loss
    optimizer = Adam(transformer.parameters(), args.lr)
    l2_loss = torch.nn.MSELoss().to(device)

    # Load style image
    style = style_transform(args.style_size)(Image.open(args.style_image))
    style = style.repeat(args.batch_size, 1, 1, 1).to(device)

    # Extract style features
    gram_style = []
    features_style = vgg(style)
    for layer_name in args.style_layer:
        ft_y = getattr(features_style, layer_name)
        gm_s = gram_matrix(ft_y)
        gram_style.append(gm_s)

    # Sample 8 images for visual evaluation of the model
    random.seed(12)
    image_samples = []
    for path in random.sample(glob.glob(f"{args.dataset_path}/*/*.jpg"), 8):
        image_samples += [style_transform(args.image_size)(Image.open(path))]
    image_samples = torch.stack(image_samples)


    def save_sample(batches_done):
        """ Evaluates the model and saves image samples """
        transformer.eval()
        with torch.no_grad():
            output = transformer(image_samples.to(device))
        image_grid = denormalize(torch.cat((image_samples.cpu(), output.cpu()), 2))
        save_image(image_grid, f"images/outputs/{style_name}-{unique_subfolder}/{batches_done}.jpg", nrow=4)
        transformer.train()

    for epoch in range(args.epochs):
        epoch_metrics = {"content": [], "style": [], "total": []}
        for batch_i, (images, _) in enumerate(dataloader):
            optimizer.zero_grad()

            images_original = images.to(device)
            images_transformed = transformer(images_original)

            # Extract features
            features_original = vgg(images_original)
            features_transformed = vgg(images_transformed)

            # Compute content loss as MSE between features
            content_loss = 0
            for layer_name in args.content_layer:
                layer = getattr(features_transformed, layer_name)
                original_layer = getattr(features_original, layer_name)
                content_loss += args.lambda_content * l2_loss(layer, original_layer)
            content_losses.append(content_loss.data.cpu().numpy())
            # Compute style loss as MSE between gram matrices
            style_loss = 0
            for layer_name, gm_s in zip(args.style_layer, gram_style):
                ft_y = getattr(features_transformed, layer_name)
                # gm_s = getattr(gram_style, layer_name)
                gm_y = gram_matrix(ft_y)
                style_loss += l2_loss(gm_y, gm_s[: images.size(0), :, :])
            style_loss *= args.lambda_style
            style_losses.append(style_loss.data.cpu().numpy())
            #total_loss = style_loss
            total_loss = content_loss + style_loss
            total_loss.backward()
            optimizer.step()

            epoch_metrics["content"] += [content_loss.item()]
            epoch_metrics["style"] += [style_loss.item()]
            epoch_metrics["total"] += [total_loss.item()]

            sys.stdout.write(
                "\r[Epoch %d/%d] [Batch %d/%d] [Content: %.2f (%.2f) Style: %.2f (%.2f) Total: %.2f (%.2f)]"
                % (
                    epoch + 1,
                    args.epochs,
                    batch_i,
                    len(train_dataset),
                    content_loss.item(),
                    np.mean(epoch_metrics["content"]),
                    style_loss.item(),
                    np.mean(epoch_metrics["style"]),
                    total_loss.item(),
                    np.mean(epoch_metrics["total"]),
                )
            )

            batches_done = epoch * len(dataloader) + batch_i + 1
            if batches_done % args.sample_interval == 0:
                save_sample(batches_done)

            if args.checkpoint_interval > 0 and batches_done % args.checkpoint_interval == 0:
                style_name = os.path.basename(args.style_image).split(".")[0]
                torch.save(transformer.state_dict(), f"checkpoints/{style_name}-{unique_subfolder}/{style_name}_{batches_done}.pth")
            if batch_i % 200 == 0:
                fig = plt.figure()
                plt.plot(style_losses, "-b", label="Style loss")
                plt.plot(content_losses, "-r", label="Feature loss")
                plt.legend(loc='upper right')
                fig.suptitle('Reconstruction loss', fontsize=16)
                plt.xlabel('Epoch', fontsize=14)
                plt.ylabel('Loss', fontsize=14)
                plt.yscale('log')
                fig.savefig(f"images/graphs/{style_name}-{unique_subfolder}/reconstruction_loss{batch_i}.jpg")