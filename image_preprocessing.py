import os, sys
import PIL.Image as Image


for infile in sys.argv[1:]:

    outfile =  os.path.splitext(infile)[0] + ".thumbnail.jpg"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im = im.crop((0,0,256,256))
            im.save(outfile, "JPEG")
        except IOError:
            print("lol")