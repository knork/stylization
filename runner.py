import os
import glob

epochs=1
batch_size=8
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("starry_night.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("starry_night.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("the_muse.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("Composition_7.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("Insta.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("cuphead.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("mosaic.jpg",epochs,batch_size))
os.system("( python3 train.py --dataset_path ../trainingdata/cropped/  --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("wave.jpg",epochs,batch_size))
# os.system("( python3 train.py --dataset_path images/tinyTrainingData/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256)"%("Moody_BW2.jpg",epochs,batch_size))

## NOTE in paper content layer used is relu3_3 but in code it is relu2_2 and i let it relu2_2
content_layers = ["relu1_2", "relu2_2", "relu3_3", "relu4_3", "relu5_3"]
style_layers = ["relu2_2,relu3_3,relu4_3,relu5_3", "relu1_1,relu2_1,relu3_1,relu4_1,relu5_1", "relu4_3,relu5_3", "relu1_2,relu2_2"]

# print('Experiments with different content layers')
# for content_layer in content_layers:
#     print('Experiment with content layer: '+content_layer)
#     os.system("( python3 train.py --dataset_path ../trainingdata/cropped/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256 --content_layer %s )"%("wave.jpg",epochs,batch_size,content_layer))
# print()
print('Experiments with different style layers')
for style_layer in style_layers:
    print('Experiment with style layer: '+style_layer)
    os.system("( python3 train.py --dataset_path ../trainingdata/cropped/ --style_image images/styles/%s --epochs %d --batch_size %d --image_size 256 --style_layer %s)"%("wave.jpg",epochs,batch_size,style_layer))

# for file in glob.glob("images/testScenes/*.png"):
#     print(file)
#     os.system(f"( python3 test_on_image.py --image_path {file}  --checkpoint_model checkpoints/first_run/Composition_7_5000.pth )")
